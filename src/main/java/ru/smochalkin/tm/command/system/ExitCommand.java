package ru.smochalkin.tm.command.system;

import ru.smochalkin.tm.command.AbstractSystemCommand;

public class ExitCommand extends AbstractSystemCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Close the program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
