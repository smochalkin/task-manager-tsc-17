package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractSystemCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractSystemCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public List<String> getCommandNames() {
        return new ArrayList<>(commands.keySet());
    }

    @Override
    public List<String> getCommandArgs() {
        return new ArrayList<>(arguments.keySet());
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(AbstractCommand command) {
        if (command instanceof AbstractSystemCommand) {
            AbstractSystemCommand systemCommand;
            systemCommand = (AbstractSystemCommand) command;
            String arg = systemCommand.arg();
            if (arg != null) arguments.put(arg, systemCommand);
        }
        String name = command.name();
        if (name != null) commands.put(name, command);
    }

}
