package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.model.Task;
import java.util.List;

public interface IProjectTaskService {

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskByProjectId(String projectId, String taskId);

    List<Task> findTasksByProjectId(String projectId);

    void removeProjectById(String projectId);

    void removeProjectByName(String name);

    void removeProjectByIndex(Integer index);

    boolean isProjectId(String id);

    boolean isProjectName(String name);

    boolean isProjectIndex(Integer index);

    boolean isTaskId(String id);

}
